import requests
import unittest


class FileHandleAPI(object):
    """
    Class provide interface for testing File Handle API with own logic
    """

    def __init__(self):
        self.endpoint_url = 'http://77.222.149.50:1310'  # endpoint rest url
        self.timeout = 60  # timeout set for all requests
        self.ok_code = [200]  # define OK status code
        self.bad_code = [400]  # define Error status code
        self.response_ok = [u'OK', 'OK']  # define OK response
        self.urls = {'endpoint': 'http://77.222.149.50:1310',  # base url settings
                     'create': '/file/create',
                     'delete': '/file/delete',
                     'list': '/file/list'}

    def f_h_test(self, method, **params):

        if not self.urls.get(method):
            print('Test stopped. Method {0} not implement. Allowed methods: {1}'.format(method, self.urls.keys()))
            raise NotImplemented

        req_param = {'url': '{0}{1}'.format(self.urls['endpoint'], self.urls[method]),
                     'params': params,
                     'timeout': self.timeout}

        req = requests.get(**req_param)

        ### Logic

        if method in ['create', 'delete']:

            if req.status_code in self.ok_code and req.text in self.response_ok:
                test = True
            else:
                if req.status_code in self.bad_code and req.text != u'':
                    test = False
                else:
                    print 'Unknown response code'
                    raise NotImplemented

        elif method in ['list']:
            if req.status_code in self.ok_code:
                test = True
            else:
                if req.status_code in self.bad_code and req.text != u'':
                    test = False
                else:
                    print 'Unknown response code'
                    raise NotImplemented
        else:
            raise NotImplemented
        return test


class APITest(unittest.TestCase):
    def setUp(self):
        self.testConnector = FileHandleAPI()

    def test_create(self):
        assert self.testConnector.f_h_test('create', username='pavlo', name='file1') == True, 'Create normal'
        assert self.testConnector.f_h_test('create', username='pavlooooooooooooo',
                                           name='file1') == False, 'Long username'
        assert self.testConnector.f_h_test('create', username='Pavlo1', name='file1') == False, 'Number in username'
        assert self.testConnector.f_h_test('create', username='petro') == False, 'Create without name'
        assert self.testConnector.f_h_test('create', name='file2') == False, 'Create without username'
        assert self.testConnector.f_h_test('create', username='pavlo', name='') == False, 'Incorect filename'
        assert self.testConnector.f_h_test('create', username='123', name='file4') == False, 'Incorrect username'
        assert self.testConnector.f_h_test('create', username='Pavlo', name='.fi.le5.') == False, 'Dot in filename'
        assert self.testConnector.f_h_test('create', username='Pavlo', name='fil.le6.') == True, 'Dot in the beg.file'
        assert self.testConnector.f_h_test('create', username='Pavlo', name='fil.Le6.') == False, 'Big Letter'

    def test_delete(self):
        assert self.testConnector.f_h_test('create', username='pavlo', name='file1') == True, 'Delete normal'
        assert self.testConnector.f_h_test('create', username='pavlooooooooooooo',
                                           name='file1') == False, 'Long username'
        assert self.testConnector.f_h_test('create', username='Pavlo1', name='file1') == False, 'Number in username'
        assert self.testConnector.f_h_test('create', username='petro') == False, 'Delete without name'
        assert self.testConnector.f_h_test('create', name='file2') == False, 'Delete without username'
        assert self.testConnector.f_h_test('create', username='pavlo', name='') == False, 'Incorect filename'
        assert self.testConnector.f_h_test('create', username='123', name='file4') == False, 'Incorrect username'
        assert self.testConnector.f_h_test('create', username='Pavlo', name='.fi.le5.') == False, 'Dot in filename'
        assert self.testConnector.f_h_test('create', username='Pavlo', name='fil.le6.') == True, 'Dot in the beg.file'
        assert self.testConnector.f_h_test('create', username='Pavlo', name='fil.Le6.') == False, 'Big Letter'

    def test_list(self):
        assert self.testConnector.f_h_test('list', username='pavlo') == True, 'List normal'
        assert self.testConnector.f_h_test('create', username='pavlooooooooooooo') == False, 'Long username'
        assert self.testConnector.f_h_test('create', username='Pavlo1') == False, 'Number in username'
