"""
Tests for File Handle API

The point is, that all tests in APITest class should return true! I am checking all requirements for REST in
FileHandleAPI Logic section, and return True if test is ok, otherwise test will crash.
"""

import requests
import unittest
import re


class FileHandleAPI(object):
    """
    Class provide interface for testing File Handle API with own logic
    """

    def __init__(self):
        self.endpoint_url = 'http://77.222.149.50:1310'  # endpoint rest url
        self.timeout = 60  # timeout set for all requests
        self.ok_code = [200]  # define OK status code
        self.bad_code = [400]  # define Error status code
        self.response_ok = [u'OK', 'OK']  # define OK response
        self.urls = {'endpoint': 'http://77.222.149.50:1310',  # base url settings
                     'create': '/file/create',
                     'delete': '/file/delete',
                     'list': '/file/list'}

        self.patterns = {'username': '^[a-zA-Z]{5,10}$',  # allowed patterns
                         'name': '^[^.][a-z0-9.]{1,15}$'}

    def f_h_test(self, method, **params):
        if not self.urls.get(method):
            print('Test stopped. Method {0} not implement. Allowed methods: {1}'.format(method, self.urls.keys()))
            raise NotImplemented
        param_assert = self.__parameters_req_check(method, **params)
        req_param = {'url': '{0}{1}'.format(self.urls['endpoint'], self.urls[method]),
                     'params': params,
                     'timeout': self.timeout}
        req = requests.get(**req_param)

        ### Logic

        if method in ['create', 'delete']:
            if param_assert:
                if req.status_code in self.ok_code and req.text in self.response_ok:
                    test = True
                    print('Status Code not 200')
                else:
                    test = False
            else:
                if req.status_code in self.bad_code and req.text:
                    test = True
                else:
                    print('Status Code not 400 or no understandable message in the body')
                    test = False

        elif method in ['list']:
            if param_assert:
                if req.status_code in self.ok_code:
                    test = True
                else:
                    print('Status Code not 200')
                    test = False
            else:
                if req.status_code in self.bad_code:
                    test = True
                else:
                    print('Status Code not 400 or no understandable message in the body')
                    test = False
        return test

    def __parameters_req_check(self, method, **params):
        test_result = False
        if method in ['create', 'delete']:
            try:
                un_match = re.match(self.patterns['username'], params.get('username', ''))
                n_match = re.match(self.patterns['name'], params.get('name', ''))
                if un_match and n_match:
                    test_result = True
            except:
                test_result = False
        elif method in ['list']:
            try:
                if re.match(self.patterns['username'], params.get('username', '')):
                    test_result = True
            except:
                test_result = False

        else:
            print('Test stopped. Method not implement')
            raise NotImplemented
        return test_result


class APITest(unittest.TestCase):
    def setUp(self):
        self.testCon = FileHandleAPI()

    def test_create(self):
        assert self.testCon.f_h_test('create', username='pavlo', name='file1'), 'Create normal'
        assert self.testCon.f_h_test('create', username='pavlooooooooooooo', name='file1'), 'Long username'
        assert self.testCon.f_h_test('create', username='Pavlo1', name='file1'), 'Number in username'
        assert self.testCon.f_h_test('create', username='petro'), 'Create without name'
        assert self.testCon.f_h_test('create', name='file2'), 'Create without username'
        assert self.testCon.f_h_test('create', username='pavlo', name=''), 'Incorect filename'
        assert self.testCon.f_h_test('create', username='123', name='file4'), 'Incorrect username'
        assert self.testCon.f_h_test('create', username='Pavlo', name='.fi.le5.'), 'Dot in filename'
        assert self.testCon.f_h_test('create', username='Pavlo', name='fil.le6.'), 'Dot in the beg.file'
        assert self.testCon.f_h_test('create', username='Pavlo', name='fil.Le6.'), 'Big Letter'

    def test_delete(self):
        assert self.testCon.f_h_test('create', username='pavlo', name='file1'), 'Delete normal'
        assert self.testCon.f_h_test('create', username='pavlooooooooooooo', name='file1'), 'Long username'
        assert self.testCon.f_h_test('create', username='Pavlo1', name='file1'), 'Number in username'
        assert self.testCon.f_h_test('create', username='petro'), 'Delete without name'
        assert self.testCon.f_h_test('create', name='file2'), 'Delete without username'
        assert self.testCon.f_h_test('create', username='pavlo', name=''), 'Incorect filename'
        assert self.testCon.f_h_test('create', username='123', name='file4'), 'Incorrect username'
        assert self.testCon.f_h_test('create', username='Pavlo', name='.fi.le5.'), 'Dot in filename'
        assert self.testCon.f_h_test('create', username='Pavlo', name='fil.le6.'), 'Dot in the beg.file'
        assert self.testCon.f_h_test('create', username='Pavlo', name='fil.Le6.'), 'Big Letter'

    def test_list(self):
        assert self.testCon.f_h_test('list', username='pavlo'), 'List normal'
        assert self.testCon.f_h_test('list', username='pavlooooooooooooo'), 'Long username'
        assert self.testCon.f_h_test('list', username='Pavlo1'), 'Number in username'
        assert self.testCon.f_h_test('list', username1='Pavlo1'), 'No username'
